#include "defs.h"
#include <stdio.h>
#include <string.h>
FILE *search_book(FILE *fp, char *title) {
  int i;
  fseek(fp, 0, SEEK_SET);
  books tmp;
  while (fread(&tmp, sizeof(books), 1, fp) != 0) {
    if (!strcmp(tmp.title, title)) {
      fseek(fp, sizeof(books) * (-1), SEEK_CUR);
      return fp;
    }
  }
  fseek(fp, 0, SEEK_SET);
  return NULL;
}

FILE *search_roll(FILE *fp, int roll) {
  fseek(fp, 0, SEEK_SET);
  students tmp;
  while (fread(&tmp, sizeof(students), 1, fp) != 0) {
    if (tmp.roll == roll) {
      fseek(fp, sizeof(students) * (-1), SEEK_CUR);
      return fp;
    }
  }
  fseek(fp, 0, SEEK_SET);
  return NULL;
}

FILE *search_issn(FILE *fp, int issn) {
  fseek(fp, 0, SEEK_SET);
  books tmp;
  while (fread(&tmp, sizeof(books), 1, fp) != 0) {
    if (tmp.issn == issn) {
      fseek(fp, sizeof(books) * (-1), SEEK_CUR);
      return fp;
    }
  }
  fseek(fp, 0, SEEK_SET);
  return NULL;
}
