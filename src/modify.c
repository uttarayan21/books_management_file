#include "defs.h"

int modify_s(FILE *fp, students std) {
  fseek(fp, 0, SEEK_SET);
  students tmp;
  while (fread(&tmp, sizeof(students), 1, fp) != 0) {
    if (tmp.roll == std.roll) {
      fseek(fp, sizeof(students) * (-1), SEEK_CUR);
      fwrite(&std, sizeof(students), 1, fp);
    }
  }
  return 0;
}

int modify_b(FILE *fp, books bk) {
  fseek(fp, 0, SEEK_SET);
  books tmp;
  while (fread(&tmp, sizeof(books), 1, fp) != 0) {

    if (tmp.issn == bk.issn) {
      fseek(fp, sizeof(books) * (-1), SEEK_CUR);
      fwrite(&bk, sizeof(books), 1, fp);
    }
  }
  return 0;
}
