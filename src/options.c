#include "defs.h"

int options_list(char type) {
  int choice;
  int issn, roll;
  char another;
  // FILE *fs, *fb;
  if (type == 's') {
    do {
      printf("\n1. Add an a student entry to the table"
             "\n2. Delete a student entry"
             "\n3. Modify a student entry"
             "\n4. List the student table"
             "\n5. Return");
      printf("\n\nEnter your choice : ");
      scanf("%d", &choice);
      __fpurge(stdin);

      switch (choice) {

      case 1:
        do {
          add_to_table(fs, 's');
          printf("Add another ? (y/n) : ");
          scanf("%c", &another);
        } while (another == 'y');
        break;
      case 2:
        printf("Enter the roll of the student entry to be deleted : ");
        scanf("%d", &roll);
        delete_from_table(fs, 's', roll);
        break;
      case 3:
        printf("Enter the roll of the student entry to be modified : ");
        scanf("%d", &roll);
        // placeholder comment to modify
        break;
      case 4:
        printf("Listing the students table : \n\n\n");
        list_table(fs, 's');
        break;

      case 5:
        printf("\nReturning to previous menu\n");
        break;
      default:
        printf("\nEnter a valid choice\n");
      }

    } while (choice != 5);

  } else if (type == 'b') {
    do {
      printf("\n1. Add an a book to the table"
             "\n2. Delete a book entry"
             "\n3. Modify a book entry"
             "\n4. List the books table"
             "\n5. Return");
      printf("\n\nEnter your choice : ");
      scanf("%d", &choice);
      __fpurge(stdin);

      switch (choice) {

      case 1:
        do {
          add_to_table(fb, 'b');
          printf("Add another ? (y/n) : ");
          scanf("%c", &another);
        } while (another == 'y');
        break;
      case 2:
        printf("Enter the issn of the book to be deleted : ");
        scanf("%d", &issn);
        delete_from_table(fb, 'b', issn);
        break;
      case 3:
        printf("Enter the issn of the book to be modified : ");
        scanf("%d", &issn);
        // placeholder comment to modify
        break;
      case 4:
        printf("Listing the books table : \n\n\n");
        list_table(fb, 'b');
        break;
      case 5:
        printf("\nReturning to previous menu\n\n");
        break;
      default:
        printf("\nEnter a valid choice\n\n");
      }

    } while (choice != 5);
  }
  return 0;
}
