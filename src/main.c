#include "defs.h"
#include <stdio_ext.h>

FILE *fs, *fb;
char file_name_books[20] = "books.txt", file_name_students[20] = "students.txt";

int main(int argc, char const *argv[]) {
  int choice, roll, issn;
  char another, title[30];
  fs = file_open(file_name_students);
  fb = file_open(file_name_books);
  do {
    if (fseek(fb, 0, SEEK_END), ftell(fb) != 0) {
      printf("1. Search book\n");
      if (fseek(fs, 0, SEEK_END), ftell(fs) != 0) {
        printf("2. Issue book\n");
        printf("3. Return book\n");
      }
    }
    if (ftell(fs) != 0)
      printf("4. Options for the students table\n");
    else
      printf("4. Add to the students table\n");
    if (ftell(fb) != 0)
      printf("5. Options for the books table\n");
    else
      printf("5. Add to the books table\n");
    printf("6. Exit\n");
    printf("Enter your choice : ");
    scanf("%d", &choice);
    __fpurge(stdin);
    switch (choice) {
    case 1:
      printf("Enter the title of the book to search : ");
      gets(title);
      books tmp;
      if (search_book(fb, title) == NULL) {
        printf("\n\nBook not found \n\n");
        break;
      } else
        fb = search_book(fb, title);
      fread(&tmp, sizeof(books), 1, fb);
      printf("Title : %s\n", tmp.title);
      printf("ISSN  : %d\n", tmp.issn);
      break;
    case 2:
      printf("Enter the issn of the book you want to issue : ");
      scanf("%d", &issn);
      printf("Enter your roll number : ");
      scanf("%d", &roll);
      issue(roll, issn);
      break;
    case 3:
      printf("Enter your roll number : ");
      scanf("%d", &roll);
      return_book(roll);
      break;
    case 4:
      if (ftell(fs) == 0)
        do {
          add_to_table(fs, 's');
          printf("Add another ? (y/n) : ");
          scanf("%c", &another);
          __fpurge(stdin);
        } while (another == 'y');
      else
        options_list('s');
      break;
    case 5:
      if (ftell(fb) == 0)
        do {
          add_to_table(fb, 'b');
          printf("Add another ? (y/n) : ");
          scanf("%c", &another);
          __fpurge(stdin);
        } while (another == 'y');
      else
        options_list('b');
      break;
    case 6:
      printf("Exiting proogram\n");
      break;
    default:
      printf("Please enter a valid choice\n");
    }
  } while (choice != 6);

  return 0;
}
