#include "defs.h"
int return_book(int roll) {
  FILE *ftmp;
  students stmp;
  books btmp;
  fs = search_roll(fs, roll);
  fread(&stmp, sizeof(students), 1, fs);
  if (stmp.book_issued == 0) {
    printf("You have not issued a book yet\n");
    return 0;
  }
  fb = search_issn(fb, stmp.book_issued);
  fread(&btmp, sizeof(books), 1, fb);
  btmp.issue_status = 'n';
  stmp.book_issued = 0;
  fseek(fs, sizeof(students) * (-1), SEEK_CUR);
  fseek(fb, sizeof(books) * (-1), SEEK_CUR);
  modify_s(fs, stmp);
  modify_b(fb, btmp);
  printf("Book returned successfully\n\n");
  return 0;
}
