#include "defs.h"
#include <stdlib.h>
int add_to_table(FILE *fp, char type) {

  fseek(fp, 0, SEEK_END);

  if (type == 's') {
    students tmp;
    printf("Enter the name of the student : ");
    gets(tmp.name);
    printf("Enter the roll number of the student : ");
    scanf("%d", &tmp.roll);
    __fpurge(stdin);
    printf("Enter the department of the student : ");
    scanf("%s", tmp.dept);
    __fpurge(stdin);
    tmp.book_issued = 0;
    fwrite(&tmp, sizeof(tmp), 1, fp);

  } else if (type == 'b') {

    books tmp;
    printf("Enter the title of the book : ");
    __fpurge(stdin);
    gets(tmp.title);
    printf("Enter the issn number of the book : ");
    scanf("%d", &tmp.issn);
    __fpurge(stdin);
    tmp.issue_status = 'n';
    fwrite(&tmp, sizeof(tmp), 1, fp);
  } else {
    printf("ERROR : There's somehing wrong with the program\n");
    exit(1);
  }

  return 0;
}
