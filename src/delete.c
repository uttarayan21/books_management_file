#include "defs.h"

int delete_from_table(FILE *fp, char type, int query) {
  FILE *ftmp;
  ftmp = fopen(".temp", "w");
  if (type == 's') {
    int roll = query;
    fseek(fp, 0, SEEK_SET);
    students tmp;
    while (fread(&tmp, sizeof(students), 1, fp) != 0) {
      if (tmp.roll == roll)
        continue;
      fwrite(&tmp, sizeof(students), 1, ftmp);
    }

    fclose(fp);
    fclose(ftmp);
    remove(file_name_students);
    rename(".temp", file_name_students);
    remove(".temp");

    fs = fopen(file_name_students, "r+");

  } else if (type == 'b') {
    int issn = query;
    fseek(fp, 0, SEEK_SET);
    books tmp;
    while (fread(&tmp, sizeof(books), 1, fp) != 0) {
      if (tmp.issn == issn)
        continue;
      fwrite(&tmp, sizeof(books), 1, ftmp);
    }

    fclose(fp);
    fclose(ftmp);
    remove(file_name_books);
    rename(".temp", file_name_books);
    remove(".temp");
    fb = fopen(file_name_books, "r+");
  } else {
    printf("Invalid type check your program\n");
  }
  return 0;
}
