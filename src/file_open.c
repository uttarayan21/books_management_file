#include <defs.h>
#include <stdio_ext.h>
#include <stdlib.h>

FILE *file_open(char *file_name) {

  char choice;
  FILE *fp;

  printf("\nOpen the default file %s ? (Y/n) :", file_name);
  scanf("%c", &choice);
  __fpurge(stdin);

  if (choice == 'n') {
    printf("Enter a file name : ");
    scanf("%s", file_name);
    __fpurge(stdin);

  } else
    printf("Opening the default file %s \n", file_name);

  if ((fp = fopen(file_name, "r+")) == NULL) {

    printf("\nWARNING: File couldn't be opened in r+ mode");
    printf("\nFalling back to w+ mode");

    if ((fp = fopen(file_name, "w+")) == NULL) {

      printf("\nERROR: File couldn't be opened\n");
      exit(1);
    } else
      printf("\nFile was successfully created \n");
  }

  return fp;
}
