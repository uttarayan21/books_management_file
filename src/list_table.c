#include "defs.h"
#include <stdio.h>
int list_table(FILE *fp, char type) {
  fseek(fp, 0, SEEK_SET);
  if (type == 's') {
    students tmp;
    while (fread(&tmp, sizeof(students), 1, fp) != 0) {
      printf("Name : %s \n", tmp.name);
      printf("Roll : %d \n\n", tmp.roll);
    }
  } else if (type == 'b') {
    books tmp;
    while (fread(&tmp, sizeof(books), 1, fp) != 0) {
      printf("Title : %s \n", tmp.title);
      printf("ISSN : %d \n\n", tmp.issn);
    }
  } else
    printf("ERROR : Ypur program is bugged !\n");
  return 0;
}
