#include "defs.h"
int issue(int roll, int issn) {
  FILE *ftmp;
  fb = search_issn(fb, issn);
  fs = search_roll(fs, roll);
  students stmp;
  books btmp;
  fread(&stmp, sizeof(students), 1, fs);
  fread(&btmp, sizeof(books), 1, fb);
  if (stmp.book_issued != 0) {
    printf("You have already issued a book\n");
    return 1;
  }
  if (btmp.issue_status == 'y') {
    printf("Book already issued\n\n");
    return 1;
  }
  btmp.issue_status = 'y';
  stmp.book_issued = btmp.issn;
  fseek(fs, sizeof(students) * (-1), SEEK_CUR);
  fseek(fb, sizeof(books) * (-1), SEEK_CUR);
  modify_s(fs, stmp);
  modify_b(fb, btmp);
  printf("Book issued successfully\n\n");
  return 0;
}
