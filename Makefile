CPP      = clang++
CC       = clang
OBJ      = obj/main.o obj/add_table.o obj/delete.o obj/file_open.o obj/issue.o obj/list_table.o obj/modify.o obj/options.o obj/search.o obj/return_book.o
LINKOBJ  = obj/main.o obj/add_table.o obj/delete.o obj/file_open.o obj/issue.o obj/list_table.o obj/modify.o obj/options.o obj/search.o obj/return_book.o
LIBS     = -static-libgcc -pg
INCS     = include/defs.h
INCLUDE  = include
BIN      = out/books_management
CXXFLAGS = $(CXXINCS) -pg
CFLAGS   = -I$(INCLUDE) -pg
RM       = rm -f

.PHONY: all all-before all-after clean clean-custom

all: all-before $(BIN) all-after

clean: clean-custom
	${RM} $(OBJ) $(BIN)

$(BIN): $(OBJ)
	$(CC) $(LINKOBJ) -o $(BIN) $(LIBS)

obj/main.o: src/main.c $(INCS)
	$(CC) -c src/main.c -o obj/main.o $(CFLAGS)

obj/add_table.o: src/add_table.c $(INCS)
	$(CC) -c src/add_table.c -o obj/add_table.o $(CFLAGS)

obj/delete.o: src/delete.c $(INCS)
	$(CC) -c src/delete.c -o obj/delete.o $(CFLAGS)

obj/file_open.o: src/file_open.c $(INCS)
	$(CC) -c src/file_open.c -o obj/file_open.o $(CFLAGS)

obj/issue.o: src/issue.c $(INCS)
	$(CC) -c src/issue.c -o obj/issue.o $(CFLAGS)

obj/list_table.o: src/list_table.c $(INCS)
	$(CC) -c src/list_table.c -o obj/list_table.o $(CFLAGS)

obj/modify.o: src/modify.c $(INCS)
	$(CC) -c src/modify.c -o obj/modify.o $(CFLAGS)

obj/options.o: src/options.c $(INCS)
	$(CC) -c src/options.c -o obj/options.o $(CFLAGS)

obj/search.o: src/search.c $(INCS)
	$(CC) -c src/search.c -o obj/search.o $(CFLAGS)

obj/return_book.o: src/return_book.c $(INCS)
		$(CC) -c src/return_book.c -o obj/return_book.o $(CFLAGS)
