#ifndef __DEFS_H__
#define __DEFS_H__

#include <stdio.h>
#include <stdio_ext.h>
//structures and variables
typedef struct books {
  char title[30],issue_status;
  int issn;
} books;

typedef struct students {
  char name[30],dept[10];
  int book_issued,roll;
} students;

int num_students,num_books;

extern char file_name_books[20];
extern char file_name_students[20];

extern FILE *fs, *fb;


//function definitions

FILE *file_open(char *);
FILE *search_book(FILE *,char *);
FILE *search_roll(FILE *,int);
FILE *search_issn(FILE *,int);
int delete_from_table(FILE*, char ,int);
int add_to_table(FILE *, char);
int options_list(char);
int modify_table(FILE *, char);
int modify_s(FILE *, students);
int modify_b(FILE *, books);

int list_table(FILE *, char) ;


#endif
